import styled from "styled-components"
import {Link} from "react-router-dom"

export const NavContainer = styled.div`
    display: flex;
    /* justify-content:space-between; */
    justify-content: flex-end;
    align-items: center;
    color: white;
    background-color: #fff;
    /* background-color: #131A22; */
    padding: 20px 10px;  
    
    .logo {
        cursor: pointer;
        margin-right: auto;
    }
    `

export const NavRight = styled.ul`
    /* display: flex;
    justify-content: 'space-between';
    align-items: center; */

    display: inline-block;
    list-style: none;
    font-family: 'Manrope', sans-serif;
    li {
        display: inline-block;
        padding: 0px 20px;
        color: #000;
        cursor: pointer;
        font-family: Inter;
        font-style: normal;
        font-weight: 600;
        font-size: 14px;
        line-height: 24px;
        /* identical to box height, or 171% */

        letter-spacing: -0.022em;
    }
    li a {
        transition: all 0.3s ease 0s;
        color: #000;
        cursor: pointer;
    }
    li a:hover {
        color: #000;
        cursor: pointer;
    }
    `

export const NavBtn = styled(Link)`
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    padding: 8px 24px;
    
    flex: none;
    order: 1;
    flex-grow: 0;
    margin: 0px 48px;

    background: #1B734F;
    border-radius: 8px;
    
    
    /*  for the text */
    font-family: Inter;
    font-style: normal;
    font-weight: 600;
    font-size: 14px;
    line-height: 16px;
    /* identical to box height, or 114% */
    
    letter-spacing: -0.022em;
    
    color: #FFFFFF;
    `
    export const HeroContainer = styled("div")`
    
    margin: 20px;
    `