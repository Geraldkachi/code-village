import React, { ReactNode } from 'react'
import { ButtonLink, ButtonStyled } from './Button/ButtonStyled'



interface ButtonStyled {
    title?: string
    primary?: any
    children: ReactNode
    to: any
    props: any
}

const Button = ({to, primary, title, children, props }: ButtonStyled) => {
    return to ? (
        <ButtonLink to={to} >{children}</ButtonLink>
     ) : (
        <ButtonStyled>{children}</ButtonStyled>
     )
}

export default Button
