/*
 * component: FlexibleDiv
 * author: FitzGerald Kachi
 * Date: June 7th 2021
 * Exam-Padi FlexibleDiv and Gridable component
 *  use props to customize where rendered
 */

import styled from "styled-components";

// Flexible box
export const FlexibleDiv = styled("div")`
  display: flex;
  justify-content: ${(props: any) => props.justifyContent || "center"};
  align-items: ${(props: any) => props.alignItems || "center"};
  flex-wrap: ${(props: any) => props.flexWrap || "wrap"};
  flex-direction: ${(props: any) => props.flexDir || "row"};
  width: ${({ width }: any) => width || "100%"};
  height: ${({ width }: any) => width || "auto"};
  background: ${(bgColor: any) => bgColor || "red"};
`;

// Gridable box
export const GridableDiv = styled("div")`
  display: grid;
  grid-template-columns: ${({ gridCol }:any) => gridCol || "1fr"};
  grid-template-rows: ${(props: any) => props.gridRow || "auto"};
  grid-gap: ${(props: any) => props.gap || "10px"};
`;
