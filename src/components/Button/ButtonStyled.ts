import styled from "styled-components"
import { Link } from 'react-router-dom';

export const ButtonLink = styled(Link)`
  /* Adapt the colors based on primary prop */
    background: ${({ primary }: any) => primary ? "#1B734F" : "#fff"};
    color: ${({ primary }: any) => primary ? "#fff" : "#1B734F"};

    font-size: 1em;
    margin: 1em;
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    padding: 8px 24px;

    position: static;
    width: 109px;
    height: 32px;
    left: 217px;
    top: 0px;

    border-radius: 8px;

    /* Inside Auto Layout */

    flex: none;
    order: 1;
    flex-grow: 0;
    margin: 0px 48px;
`;

export const ButtonStyled = styled.button``