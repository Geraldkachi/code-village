import React from 'react'
import { NavContainer, NavRight, NavBtn, HeroContainer } from './NavBarStyled'
import { ReactComponent as CodeVillageIcon } from './assets/CodeVillage-Logo.svg'
import { ButtonLink } from './Button/ButtonStyled'
import { ReactComponent as HeroImage } from '../assets/Frame 7.svg'
import { GridableDiv } from './Box/styles'


const NavBarComponent = () => {
    return (
        <>
        <NavContainer>
            <CodeVillageIcon className="logo" />
            <nav>
                <NavRight>
                    <li>Work</li>
                    <li>About</li>
                    <li>School</li>
                </NavRight>
            </nav>
            <NavBtn to="/">Let’s Talk</NavBtn>
        </NavContainer>

        {/* hero view */}
        <GridableDiv>
            <HeroContainer className="h">
                <HeroImage className="heroimg" />
                <div>We are a software development company that provides
                     state-of-the-art solutions for startups and enterprises.
                      We focus on long-term partnerships and products that make
                       people’s lives better.</div>
            </HeroContainer>
        </GridableDiv>
        </>
    )
}

export default NavBarComponent
